import { ArtistComponent } from "./components/artist/artist.component";

export const ArtistRouting = [{
    path: '',
    component: ArtistComponent
}];