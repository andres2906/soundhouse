import { Injectable } from '@angular/core';

@Injectable()
export class StorageManagerService {

  constructor() { }

    public store(name: string, content: any) {
        if ( (content instanceof Object) && (content !== null) ) {
            localStorage.setItem(name, JSON.stringify(content));
        } else {
            localStorage.setItem(name, content);
        }
    }

    public retrieve(name: string) {
        let stored: any = localStorage.getItem(name);
        stored = (stored == null )  ? '' : stored;
        return stored;
    }

    public remove(name: string) {
        localStorage.removeItem(name);
    }

  }
