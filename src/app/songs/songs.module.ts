import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SongsComponent } from './components/songs/songs.component';
import { RouterModule } from '@angular/router';
import { SongsRouting } from './songs.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SongsRouting)
  ],
  declarations: [SongsComponent]
})
export class SongsModule { }
