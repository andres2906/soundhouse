import { Component, OnInit } from '@angular/core';
import { Genre } from '../../models/Genre.model';
import { GenreService } from '../../services/Genre.service';
import { UserService } from 'src/app/layout/services/user.service';
import { FirebaseUserModel } from 'src/app/layout/models/user.model';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { AppStateService } from '../../../core/services/AppState.service';

@Component({
  selector: 'app-onboard',
  templateUrl: './onboard.component.html',
  styleUrls: ['./onboard.component.scss']
})
export class OnboardComponent {
  genresList: Genre[];
  userName: String;
  genresSelected = [];

  onActiveChanged(event: any) {
    if (!event.isActive) {
      var index = this.genresSelected.indexOf(event.title);
      if (index > -1) {
        this.genresSelected.splice(index, 1);
      }
    } else {
      this.genresSelected.push(event.title);
    }
    console.log(this.genresSelected);
  }

  savePreferences() {
    this.appState.setPreferences(this.genresSelected);
    this.router.navigate(['home/welcome']);
  }

  constructor(private GenreService: GenreService, private router: Router, private appState: AppStateService) {
    if (this.appState.getUser() === null) {
      this.router.navigate(['/landing']);
    } else {
      let userPreferences = null;
      userPreferences = this.appState.getUserPreferences();
      if (userPreferences === null) {
        this.appState.setNotFoundPreferences(true);
        this.router.navigate(['/home/onboard']);
      } else {
        this.appState.setNotFoundPreferences(false);
        this.router.navigate(['home/welcome']);
      }
    }


    this.userName = this.appState.getUser().userName;

    /*
    var x = this.GenreService.getData();
    x.snapshotChanges().subscribe(item => {
      this.genresList = [];
      item.forEach(element => {
        var y = element.payload.toJSON();
        this.genresList.push(y as Genre);
      });
    });
    */


   this.genresList = [];

   this.genresList.push(new Genre("African music",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/african.jpg?alt=media"));

   this.genresList.push(new Genre("Brazilian music",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/brazilian.jpg?alt=media"));

   this.genresList.push(new Genre("Blues",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/blues.jpg?alt=media"));

   this.genresList.push(new Genre("Christian music",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/christian.jpg?alt=media"));

   this.genresList.push(new Genre("Classical",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/classical.jpg?alt=media"));

   this.genresList.push(new Genre("Colombian music",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/colombian.jpg?alt=media"));

   this.genresList.push(new Genre("Dance & EDM",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/dance.jpg?alt=media"));

   this.genresList.push(new Genre("Electronic",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/electronic.jpg?alt=media"));

   this.genresList.push(new Genre("Folk & Singer - Songwriter",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/folk.jpg?alt=media"));

   this.genresList.push(new Genre("Indie",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/indie.jpg?alt=media"));

   this.genresList.push(new Genre("Jazz",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/jazz.jpg?alt=media"));

   this.genresList.push(new Genre("Latin music",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/latin.jpg?alt=media"));

   this.genresList.push(new Genre("Merengue",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/merengue.jpg?alt=media"));

   this.genresList.push(new Genre("Metal",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/metal.jpg?alt=media"));

   this.genresList.push(new Genre("Mexican music",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/mexican.jpg?alt=media"));

   this.genresList.push(new Genre("Pop",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/pop.jpg?alt=media"));

   this.genresList.push(new Genre("Rap",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/rap.jpg?alt=media"));

   this.genresList.push(new Genre("Reggae",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/reggae.jpg?alt=media"));

   this.genresList.push(new Genre("Reggaeton",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/reggaeton.jpg?alt=media"));

   this.genresList.push(new Genre("R n’ B",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/rnb.jpg?alt=media"));

   this.genresList.push(new Genre("Rock",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/rock.jpg?alt=media"));

   this.genresList.push(new Genre("Salsa",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/salsa.jpg?alt=media"));

   this.genresList.push(new Genre("Soul & Funk",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/soul.jpg?alt=media"));

   this.genresList.push(new Genre("TV, Gaming & Movies",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/tvgaming.jpg?alt=media"));

   this.genresList.push(new Genre("Vallenato",
           "https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/vallenato.jpg?alt=media"));


  }

}
