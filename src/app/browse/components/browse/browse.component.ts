import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { video } from '../../../core/models/video.model';
import { AppStateService } from '../../../core/services/AppState.service';
var youtube = require('youtube-finder');
var client = youtube.createClient({ key: environment.youtube.apiKey });

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['browse.component.scss']
})
export class BrowseComponent implements OnInit {
  videos : video[];
    loaded : boolean;
    API_KEY : string;
    selectVideo : video;
    lastSelection: any;
    opts : any;
    constructor(private appState: AppStateService) {

    }
    ngOnInit() {
        this.SearchValueChange('Kany');
    }
    SearchValueChange(event:any) {
      var params = {
          part: 'snippet',
          q: event.value,
          maxResults: 20
        }
        client.search(params, (err:any, data:any) => {
          // your magic..
          console.dir(data);
          this.videos = data.items;
          this.selectVideo = data.items[2];
      });
    }

    addToMySongs() {
    }



  }