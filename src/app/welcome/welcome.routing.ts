import { WelcomeComponent } from './components/welcome/welcome.component';

export const WelcomeRouting = [
    {
        path: '',
        component: WelcomeComponent
    }
]