import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RootComponent } from './components/root/root.component';
import { LoginComponent } from './components/login/login.component';
import { layoutRouting } from './layout.routing';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HomeComponent } from './components/home/home.component';
import { SplashComponent } from './components/splash/splash.component';
import { MdcButtonModule, MdcDrawerModule, MdcIconModule, MdcListModule, MdcTextFieldModule }  from '@angular-mdc/web';
import { SidebarComponent } from './components/sidebar/sidebar.component';

import { environment } from '../../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { UserResolver } from './models/user.resolver';
import { AuthGuard } from './services/auth.guard';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    MdcButtonModule,
    MdcButtonModule,
    MdcDrawerModule,
    MdcIconModule,
    MdcListModule,
    MdcTextFieldModule,
    RouterModule.forRoot(layoutRouting,  {
      useHash: true,
      enableTracing: false,
      scrollPositionRestoration: 'enabled'
    }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
    AngularFireDatabaseModule,

  ],
  exports: [RouterModule, RootComponent],
  declarations: [RootComponent, LoginComponent, HomeComponent, SplashComponent, SidebarComponent],
  providers: [AuthService, UserService, UserResolver, AuthGuard]

})
export class LayoutModule { }
