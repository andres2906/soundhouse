import { Injectable } from '@angular/core';
import { StorageManagerService } from './StorageManager.service';
const OBJECT_PREFERENCE = 'preferences';

@Injectable()
export class AppStateService {
  private userName = null;
  private userID = null;
  private notFoundPreferences = true;


  constructor(public storageManager: StorageManagerService) {
  }


  setUser(uid: string, userName: string) {
    this.userID = uid;
    this.userName = userName;
    this.storageManager.store('usuario', { uid: this.userID , userName : this.userName });
  }

  getUser() {
    let result = null;
    if (this.userName === null && this.userID ===  null) {
     result = this.storageManager.retrieve('usuario');
     return (result === '') ? null : JSON.parse(result);
    } else {
      result = { uid: this.userID , userName : this.userName };
      return result;
    }
  }

  getUserPreferences() {
    let result = null;
    result = this.storageManager.retrieve(this.userID + OBJECT_PREFERENCE);
    return (result === '') ? null : JSON.parse(result);
  }


  setPreferences(preferences) {
    this.storageManager.store(this.userID + OBJECT_PREFERENCE, preferences );
  }



  setNotFoundPreferences(value ) {
    this.notFoundPreferences = value;
  }


}
