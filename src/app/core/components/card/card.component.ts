import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() image: string;
  @Input() title: string;
  @Output() activeChanged = new EventEmitter<any>();

  public isActive = false;

  constructor() { }

  ngOnInit() {
  }

  public clickActive() {
    this.isActive = !this.isActive;
    this.activeChanged.emit({ title : this.title, isActive: this.isActive });
  }

}
