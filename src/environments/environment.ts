// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCy11ummeajezIWp3XWF9x5lDz3rbYzEjs",
    authDomain: "soundhouse-2b997.firebaseapp.com",
    databaseURL: "https://soundhouse-2b997.firebaseio.com",
    projectId: "soundhouse-2b997",
    storageBucket: "soundhouse-2b997.appspot.com",
    messagingSenderId: "387537561487"
  },
  filesystem: 'https://firebasestorage.googleapis.com/v0/b/soundhouse-2b997.appspot.com/o/african.jpg?alt=media',
  youtube: {
    apiKey : 'AIzaSyC1dIO7AaTx71ig5BXOWHZOVXeaAw5bzPM'
  }
};






/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
