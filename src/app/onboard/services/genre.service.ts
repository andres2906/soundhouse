import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database'

@Injectable()
export class GenreService {
  genreList: AngularFireList<any>;
  constructor(private firebase :AngularFireDatabase ) { }

  getData(){
    this.genreList = this.firebase.list('genders');
    return this.genreList;
  }

}
