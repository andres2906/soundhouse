import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RootComponent } from './layout/components/root/root.component';
import { LayoutModule } from './layout/layout.module';
import { CoreModule } from './core/core.module';
import { StorageManagerService } from './core/services/StorageManager.service';
import { AppStateService } from './core/services/AppState.service';


@NgModule({
  declarations: [
  ],
  imports: [
    BrowserModule,
    CoreModule,
    LayoutModule
  ],
  bootstrap: [RootComponent],
  providers: [StorageManagerService, AppStateService]
})
export class AppModule { }
