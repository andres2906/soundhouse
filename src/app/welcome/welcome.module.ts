import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { RouterModule } from '@angular/router';
import { WelcomeRouting } from './welcome.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(WelcomeRouting)
  ],
  declarations: [WelcomeComponent]
})
export class WelcomeModule { }
