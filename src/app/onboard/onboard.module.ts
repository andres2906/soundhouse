import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnboardComponent } from './components/onboard/onboard.component';
import { RouterModule } from '@angular/router';
import { onBoardRouting } from './onboard.routing';
import { CoreModule } from '../core/core.module';
import { GenreService } from './services/Genre.service';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    RouterModule.forChild(onBoardRouting),

  ],
  declarations: [OnboardComponent],
  providers: [GenreService]
})
export class OnboardModule { }
