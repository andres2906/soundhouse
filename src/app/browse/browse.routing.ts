import { BrowseComponent } from "./components/browse/browse.component";

export const BrowseRouting = [{
    path: '',
    component: BrowseComponent
}];
