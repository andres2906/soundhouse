import { Component, OnInit } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { FirebaseUserModel } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import { AppStateService } from '../../../core/services/AppState.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements Resolve<FirebaseUserModel>  {

  constructor(public userService: UserService, private router: Router, private appState: AppStateService) {
    if (this.appState.getUser() === null) {
      this.router.navigate(['/landing']);
    } else {
      let userPreferences = null;
      userPreferences = this.appState.getUserPreferences();
      if (userPreferences === null) {
        this.appState.setNotFoundPreferences(true);
        this.router.navigate(['/home/onboard']);
      } else {
        this.appState.setNotFoundPreferences(false);
        this.router.navigate(['/home/welcome']);
      }
    }
  }


  resolve(route: ActivatedRouteSnapshot) : Promise<FirebaseUserModel> {

    let user = new FirebaseUserModel();

    return new Promise((resolve, reject) => {
      this.userService.getCurrentUser()
      .then(res => {
        if(res.providerData[0].providerId == 'password') {
          user.image = 'http://dsi-vd.github.io/patternlab-vd/images/fpo_avatar.png';
          user.name = res.displayName;
          user.provider = res.providerData[0].providerId;
          return resolve(user);
        }
        else{
          user.image = res.photoURL;
          user.name = res.displayName;
          user.provider = res.providerData[0].providerId;
          return resolve(user);
        }
      }, err => {
        this.router.navigate(['/landing']);
        return reject(err);
      })
    })
  }

}
