import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowseComponent } from './components/browse/browse.component';
import { RouterModule } from '@angular/router';
import { BrowseRouting } from './browse.routing';
import { MdcTabModule, MdcTabBarModule, MdcMenuModule, MdcListModule } from '@angular-mdc/web';

@NgModule({
  imports: [
    CommonModule,
    MdcTabModule,
    MdcTabBarModule,
    MdcMenuModule,
    MdcListModule,
    RouterModule.forChild(BrowseRouting),
  ],
  declarations: [BrowseComponent]
})
export class BrowseModule { }
