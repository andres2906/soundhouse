import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QueueComponent } from './components/queue/queue.component';
import { RouterModule } from '@angular/router';
import { PlaylistRouting } from './playlist.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PlaylistRouting)
  ],
  declarations: [QueueComponent]
})
export class PlaylistModule { }
