import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-splash',
  template: `
    <p>
      splash works!
    </p>
  `,
  styles: []
})
export class SplashComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
