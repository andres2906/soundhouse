import { LoginComponent } from "./components/login/login.component";
import { HomeComponent } from "./components/home/home.component";

export const layoutRouting = [{
    path: '',
    redirectTo : 'landing',
    pathMatch : 'full'
    }, {
        path: 'landing',
        component: LoginComponent
     }, {
        path: 'home',
        component: HomeComponent,
        children: [
            {
                path: 'onboard',
                loadChildren: '../onboard/onboard.module#OnboardModule'
            },
            {
                path: 'welcome',
                loadChildren: '../welcome/welcome.module#WelcomeModule'
            },
            {
                path: 'browse',
                loadChildren: '../browse/browse.module#BrowseModule'
            },
            {
                path: 'songs',
                loadChildren: '../songs/songs.module#SongsModule'
            },
            {
                path: 'artist',
                loadChildren: '../artist/artist.module#ArtistModule'
            },
            {
                path: 'playlist',
                loadChildren: '../playlist/playlist.module#PlaylistModule'
            }
        ]
    }, {
        path: '**',
        redirectTo: 'home'
    }];
