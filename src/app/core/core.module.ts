import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card/card.component';
import { StorageManagerService } from './services/StorageManager.service';
import { AppStateService } from './services/AppState.service';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [CardComponent],
  declarations: [CardComponent],
  providers: []
})
export class CoreModule { }
