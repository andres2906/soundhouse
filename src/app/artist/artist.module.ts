import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArtistComponent } from './components/artist/artist.component';
import { RouterModule } from '@angular/router';
import { ArtistRouting } from './artist.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ArtistRouting)
  ],
  declarations: [ArtistComponent]
})
export class ArtistModule { }
